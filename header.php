<?php 
    const IMG = 'assets/img/';
?>

<!DOCTYPE html>
<html class="no-js" lang="pl">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title></title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    
    <!-- Check if JS is enabled, remove html "no-js" class    -->
    <script>(function (html) {
        html.className = html.className.replace(/\bno-js\b/, 'js')
      })(document.documentElement);</script>

    <?php
        // Add dynamic body body class based on current script name
        $body_class = null;
        if ( isset( $_SERVER['SCRIPT_NAME'] ) ) {
            $script_name = explode( '/', $_SERVER['SCRIPT_NAME'] );
            $filename    = $script_name[ count( $script_name ) - 1 ];
            $filename    = explode( '.', $filename )[0];
            $body_class  = $filename;
        }
    ?>

    <!-- external styles here -->

    <link rel="stylesheet" href="https://use.typekit.net/mxt0jbh.css">
    <!-- /external styles here -->

    <?php
        if ( file_exists( __DIR__ . '/assets/css/style.min.css' ) ) {
            echo '<link rel="stylesheet" href="assets/css/style.min.css">';
        }
        if ( file_exists( __DIR__ . '/assets/css/style.css' ) ) {
            echo '<link rel="stylesheet" href="assets/css/style.css">';
        }
    ?>
    
    <script
        src="https://code.jquery.com/jquery-3.4.1.min.js"
        integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
        crossorigin="anonymous">        
    </script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>


    <?php
        if ( file_exists( __DIR__ . '/assets/js/main.min.js' ) ) {
            echo '<script src="assets/js/main.min.js" defer></script>';
        }
        if ( file_exists( __DIR__ . '/assets/js/main.js' ) ) {
            echo '<script src="assets/js/main.js" defer></script>';
        }
    ?>

</head>
<body class="<?= $body_class ?>">
<!--[if lt IE 11]>
<p>Używasz bardzo starej wersji przeglądarki Internet Explorer. Z&nbsp;tego też powodu ta&nbsp;strona najprawdopodobniej nie&nbsp;działa prawidłowo oraz narażasz się na potencjalne ryzyko związane z bezpieczeństwem.
  <a href="https://www.microsoft.com/pl-pl/download/Internet-Explorer-11-for-Windows-7-details.aspx" rel="noopener noreferrer">Wejdź tutaj, aby ją zaktualizować.</a>
</p>
<![endif]-->
<main>


<header id="header" class="header">
    <div class="header__container">
        <div class="header__border">
            <div class="header__box">
                <a href="#!"><div class="header__logo"></div></a>
                <div class="header__social">
                    <a href="#!"  class="header__insta"></a>
                    <a href="#!" class="header__fb"></a>
                </div>
                <div class="header__country">
                    <a href="#!">PL</a>
                    <a href="#!">EN</a>
                </div>
            </div>
            <div class="header__navbox">
                <div class="nav">
                    <div class="icon">
                        <div class="hamburger"></div> 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <aside class="off">
        <nav class="nav-list">
            <ul>
                <a href="#header"><li>Menu</li></a>
                <a href="#about"><li>About</li></a>
                <a href="#realization"><li>Team</li></a>
                <a href="#contact"><li>Contact</li></a>
            </ul>
        </nav>
    </aside>
</header>











