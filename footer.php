<footer class="footer">
    <div class="footer__container">
        <div class="footer__border">
            <div class="footer__box">
                <div class="footer__col">
                    <a href="#!"><div class="footer__logo footer__logo--one"></div></a>
                    <div class="footer__social">
                        <a href="#!"><div class="footer__insta"></div></a>
                        <a href="#!"> <div class="footer__fb"></div></a>
                    </div>
                </div>
                <div class="footer__col">
                    <ul class="footer__list">
                        <a href="#!"><li class="footer__list-el">sansara group</li></a>
                        <a href="#!"><li class="footer__list-el">misja & wizja</li></a>
                        <a href="#!"><li class="footer__list-el"id="list-element">oferta</li></a>
                        <a href="#!"><li class="footer__list-el">realizacje</li></a>
                        <a href="#!"><li class="footer__list-el">kontakt</li></a>
                    </ul>
                </div>
                <div class="footer__col">
                    <a href="#!"><div class="footer__logo footer__logo--two"></div></a>
                </div>
            </div>
        </div>
    </div>
</footer>

</main>
</body>
</html>
