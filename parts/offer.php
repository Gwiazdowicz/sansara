<div class="offer">
    <div class="offer__container">
        <div class="offer__border">
            <div class="offer__title-box">
                <h2 class="offer__title a-title-two">time to unpack </h2>
                <p class="offer__number a-number">02</p>
            </div>
            <div class="offer__subtitle-box"> 
                <h3 class="offer__subtitle">Decydując się na współpracę z nami, w pakiecie otrzymasz niezwykłe przeżycia, które będą Ci towarzyszyć na każdym etapie organizacji eventu:</h3>
            </div>
            <div class="offer__box">
                <div class="offer__tile">
                    <div class="offer__ball"></div>
                    <div class="offer__img offer__img--one"></div>
                    <div class="offer__text-box">
                        <h3 class="offer__header a-header">Komfort pracy</h3>
                        <div class="offer__paddle"></div>
                        <p class="offer__text a-text">Możesz spać spokojnie. W tym czasie my zajmiemy się każdym szczegółem organizacji, abyś Ty mógł zająć się swoimi sprawami.</p>
                    </div>
                </div>
                <div class="offer__tile">
                    <div class="offer__ball-two"></div>
                    <div class="offer__img offer__img--two"></div>
                    <div class="offer__text-box-two">
                        <h3 class="offer__header a-header">Bezpieczeństwo</h3>
                        <div class="offer__paddle"></div>
                        <p class="offer__text a-text">Możesz zaufać naszemu doświadczeniu. Zapobiegamy wszelkiemu ryzyku, które może pojawić się podczas organizacji Twojego eventu.</p>
                    </div>
                </div>

                <div class="offer__tile">
                    <div class="offer__ball-three"></div>
                    <div class="offer__img offer__img--three"></div>
                    <div class="offer__text-box-three">
                        <h3 class="offer__header a-header">Uznanie</h3>
                        <div class="offer__paddle"></div>
                        <p class="offer__text a-text">Zawsze bierzemy pod uwagę Twoje pomysły, które mogą uczynić event jeszcze lepszym.</p>
                    </div>
                </div>

                <div class="offer__tile">
                    <div class="offer__ball-four"></div>
                    <div class="offer__img offer__img--four"></div>
                    <div class="offer__text-box-four">
                        <h3 class="offer__header a-header">odliczanie</h3>
                        <div class="offer__paddle"></div>
                        <p class="offer__text a-text">Wyczekujesz już swojego eventu? My też! Możesz być pewien, że będzie jedyny w swoim rodzaju.</p>
                    </div>
                   
                </div>

                <div class="offer__tile">
                    <div class="offer__img offer__img--five"></div>
                    <div class="offer__text-box-five">
                        <h3 class="offer__header a-header">entuzjazm</h3>
                        <div class="offer__paddle offer__paddle--last"></div>
                        <p class="offer__text a-text">Dzisiaj Twój event! Poczuj nieograniczoną radość i ciesz się ze swojego wydarzenia, a my będziemy czuwać nad jego przebiegiem.</p>
                    </div>
                </div>

                <div class="offer__tile">
                    <div class="offer__ball-five"></div>
                    <div class="offer__img offer__img--six"></div>
                    <div class="offer__text-box-six">
                        <h3 class="offer__header a-header">sukces</h3>
                        <div class="offer__paddle offer__paddle--last"></div>
                        <p class="offer__text a-text">Event przebiegł według planu,<br> a Ty możesz odetchnąć z satysfakcją, bo wszystko się udało.</p>
                    </div>    
                </div>
            </div>
        </div>
    </div>
</div>

