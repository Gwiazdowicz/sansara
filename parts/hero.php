<div id="hero" class="hero">
    <div class="hero__container">
            <div class="hero__box">
                <h2 class="hero__top-title a-top-title">Get this special feeling</h2>
                <h1 class="hero__title a-title">unpack your event</h1>
                <a href="#!" class="hero__btn "><p class="hero__btn-text a-btn">zacznij współpracę</p></a>
                <div class="hero__scroll_box">
                    <div id="#draggable" class="hero__scroll"><p>scroll to unpack</p></div>
                </div>
            </div>
        </div>
    </div>
</div>






