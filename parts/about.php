<div id="about" class="about">
    <div class="about__ball"></div>
    <div class="about__ball-two"></div>
    <div class="about__ball-three"></div>
    <div class="about__container">
        <div class="about__border">
            <div class="about__tile">
                <div class="about__logo about__logo--one"></div>
                <p class="about__text a-text">Ride by Sansara to agencja eventowa, która zajmuje się 
                    kompleksową organizacją eventów na każdym etapie współpracy. 
                    Nasz zespół składa się z doświadczonych specjalistów z dziedziny 
                    logistyki, cateringu, oświetlenia, dźwięku, marketingu oraz kreacji. 
                    Wieloletnie doświadczeniei pasja sprawiają, że stworzymy 
                    dla Ciebie każdy rodzaj eventu.
                </p>
            </div>
            <div class="about__tile">
                <div class="about__logo about__logo--two"></div>
                <div class="about__discription">
                    <p class="about__text-discription a-text">Sansara Group to agencja eventowa, która zajmuje się  kompleksową organizacją eventów na każdym etapie współpracy. Poznaj właścicileki agencji:</p>
                    <div class="about__employees">
                        <div class="about__person">
                            <p class=about__position>Dyrektor Kreatywny</p>
                            <div class="about__paddle"></div>
                            <div class="about__name">małgorzata mańkowska:</div>
                            <div class="about__status">
                                <p>check</p>
                            </div>
                        </div>
                        <div class="about__person">
                            <p class=about__position>Dyrektor operacyjny</p>
                            <div class="about__paddle about__paddle--two"></div>
                            <div class="about__name">Katarzyna Hącia:</div>
                            <div class="about__status">
                                <p>check</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>






