<div id="contact" class="contact">
    <div class="contact__container">
        <div class="contact__boarder">
            <div class="contact__box">
                <div class="contact__ball"></div>
                <div class="contact__form">
                    <p class="contact__form-title">Masz pytania<span>?</span></p>
                    <form>
                        <input placeholder="imię i nazwisko" type="text" />
                        <input placeholder="e-mail" type="text" />
                        <input placeholder="numer telefonu" type="text" />
                        <textarea id="textarea">treść wiadomości</textarea>
                        <input class="contact__btn" type="submit" value="Wyślij"> 
                    </form>
                </div>
                <div class="contact__info">
                    <div class="contact__title-box">
                        <h2 class="contact__title a-title-three">Kontakt</h2>  
                        <p class="contact__number a-number">05</p> 
                    </div>
                    <div class="contact__person">
                        <h3 class="contact__name about__name">małgorzata mańkowska</h3>
                        <p class="contact__data">m.mankowska@sansara-event.pl <br> +48 721 160 869 </p>
                    </div>

                    <div class="contact__person">
                        <h3 class="contact__name  about__name">Katarzyna Hącia</h3>
                        <p class="contact__data">k.hacia@sansara-event.pl <br> +48 721 140 905 </p>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</div>