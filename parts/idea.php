<div class="idea">
    <div class="idea__container">
        <div class="idea__border">
            <div class="idea__tile">
                <div class="idea__ball"></div>
                <div class="idea__col-one">
                    <p class="idea__text a-text">Wierzymy, że kreatywność i postęp nie mają granic, a wychodzenie pozaramy otwiera wiele możliwości, dlatego tworzymy eventy, w którychpodstawą jest dostarczanie niezapomnianych emocji. W naszychprodukcjach dążymy do kreowania zaskakującej i porywającej przestrzeni, w której uczestnicy eventu mogą poczuć się wyjątkowo i niepowatrzalnie.Multidyscyplinarny zespół, odwaga i nastawienie na sukces to fundamentynaszych działań.</p>
                </div>
                <div class="idea__paddle"></div>
                <div class="idea__col-two"> 
                    <div>
                        <p class="idea__title"><span>01</span>  misja</p>
                        <p class="idea__subtitle"><span>02</span> wizja</p>
                    </div>
                    <a href="#!"><div class="idea__arrow"></div></a>
                </div>
            </div>
        </div>
    </div>
</div>

