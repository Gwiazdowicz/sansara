<div id="realization" class="realization">
    <div class="realization__container">
        <div class="realization__boarder">
            <div class="realization__box">
                <div class="realization__tile">
                    <div class="realization__title-box">
                        <h2 class="realization__title a-title-three">Realizacje</h2>
                        <p class="realization__number a-number">03</p>
                    </div>
                    <div class="realization__subtitle-box">
                        <p class="realization__subtitle">nasze projekty zrealizowane w ostanim czasie:</p>
                    </div>
                    <a   href="#!"><p class="realization__btn">Zobacz projekty</p></a>
                </div>

                <div class="realization__tile-holder">
                    <div class="realization__tile">
                        <div class="realization__title-box-two">
                            <h2 class="realization__title  a-title-three a-title-three--white">oferta</h2>
                            <p class="realization__number a-number ">04</p>
                        </div>    
                        <div class="realization__subtitle-box">
                            <p class="realization__subtitle-two"> Zobacz co możemy dla Ciebie zrobić i znajdź rodzaj eventu, który Cię interesuje:</p>
                        </div>    
                        <a   href="#!"><p class="realization__btn">Zobacz ofertę</p></a>
                    </div>
                </div>    
            </div>
        </div>
    </div>
</div>