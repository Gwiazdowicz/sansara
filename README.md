# Sansara

Link to page: 
http://www.galacticshop.cba.pl/

Open .env file
Set APP_BASE to current subfolder
In root folder, in terminal, run: php core/set-base.php


TL;DR
These will get you up and running:


yarn dev or npm run dev - no build, no minification, sets up dev server proxy with live reload at localhost:3000.

yarn build:pretty or npm run build:pretty - builds the files, but without minification, keeps the console logs.

yarn build or npm run build - optimized production build.

yarn lint or npm run lint - run linters against *.js files.

yarn lintfix or npm run lintfix - run linters against *.js files and try to fix autofixable errors.

When in dev mode, you need to access the site via the proxy created by Browser-sync (by default under localhost:3000), otherwise, you'll see no styles/scripts (because there are none). Read more below.
SASS folder is structured with Atomic Design CSS methodology in mind.


Project structure
wp-boilerplate
| - _src - where the source files live 
     \ - compiler - webpack related stuff
     \ - dev-assets - here you put all your goodies
            \ - fonts
            \ - img
            \ - sass
            \ - scripts
            \ - vendor
            \ - video
            app-entry.js
Important notes:


app-entry.js is only for importing the main sass/js files. No code there please.
when accessing assets in sass/scss, you can use these aliases (ie. background-image: url(~img/...);)


~img for images folder

~fonts for fonts folder

~vendor for vendor folder

~video for video folder


when running in dev mode, there are no assets (except images) being built and moved to the assets folder. Everything is being injected dynamically by Browser-sync. That's why it is needed to access the site via the proxy.
if you want to change browser support for Autoprefixer and Babel - please find "browserslists" key in package.json file and change it how it suits.

running npx browserslits will run and show the query with all the browsers targeted by given config



Build process:
All assets are processed by Webpack.
If you need to change how is the project structured, basic config variables are available here: _src/project.config.js